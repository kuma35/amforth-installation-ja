.. -*- coding: utf-8; mode: rst; -*-

.. include:: definition.txt

=====================================
amforth インストール クイックスタート
=====================================

用意するハードウェア
====================

PC
  今回はubuntu 12.04LTS(amd64)をインストールしたマシンを使っています。
Arduino uno R3
  オープンハードなので自作しても構いません。千石電商では3000円弱(2013/3月)。
USB A-Bケーブル
  Arduinoには付属してないので。今回は50cmぐらいのを近所のダイソーで買いました。100円。
ATmega328P-PU
  atmel社のICです。Arduino uno R3に載っているのと同じものです。千石電商では300円ぐらい(2013/3月)。
  秋月電子通商では250円のようです。
  末尾のPUはパッケージの形態を示しています。ここが違うとICソケットに刺せなくなるので注意。
  Arduinoのチップを直接書き換えるなら購入しなくても構いません。
AVRISPmkII
  ISP(In System Programer)と呼ばれるものです。Arduino上のATmega328P-PUにあらかじめ書き込まれた
  ブートローダーを介さずに書き込みを行う為に利用します。ISPは通常のプログラマ(ROMライタ)に比べると
  Arduino上にICを装着したまま書き換えるのと、比較的安価なのが魅力です。一方、ATmega専用になってしまう
  ので、既にATmeagaにも対応したプログラマ(ROMライタ)を持っている人はそちらを使って構いません。
  AVRISPmkIIはUSBケーブル付属しています。秋月電子通商では3000円(2013/3月)。
IC引き抜き工具
  あれば便利。手元にあるのはaitendoで買ったもの。200円ぐらい。

ROMライタ＆書き込みソフト準備
=============================

ROMライタ(プログラマ)を用意します。ここでは |avrisp2| を使っています。
パラレルプログラマ等を利用する場合でもヒューズビット、ロックビット、hexファイルの転送は同じです。
以後の説明は |avrisp2| 前提になっているので適宜読み替えてください。

次にご利用になるISPに対応した書き込みソフトを準備します。今回は |avrisp2| を使ったので
|avrisp2| の場合は |avrdude| を準備します。aduino-1.0.3を入手した場合は  arduino-1.0.3/hardware/tools にあります。 

手元のubuntuが64bits版なので avrdude64 を使いました。

書き込みファイル準備
====================

amforthをサイトから取得します。

サイトは http://amforth.sourceforge.net/ ダウンロードはサイトからDOWNLOADリンクで辿れます ( http://sourceforge.net/projects/amforth/ )

ダウンロードした場合は展開すると、 appl/arduino の下に \*.hex ファイルが入っています(ソースの場合はmakeが必要です(後述))。

準備
====

|uno| のICをIC引き抜き工具等で引き抜き、書き込みしたいATmega328P-PUをセットします。

ISPを接続します。 |uno| は角にある白い丸印が1番ピン示していますので、これとISPのケーブルの1番ピンの位置を合わせます。

|avrisp2| の場合はターゲットのボード側にも電源供給が必要なので、 |avrisp2| のUSBケーブルに加えて |uno| のUSBケーブルも接続します。

ヒューズビット(fuse)、ロックビット(lock)設定
============================================

|avrdude| を -P usb で使うためには root権限が必要です(udevの設定も色々試したが分からなかった)。

ヒューズビット等設定用に edit_uno.sh を定義します。例えば以下のような感じです。

.. code-block:: bash
   
   #!/bin/bash
   HARDWARE_TOOLS=$HOME/.arduino/arduino/hardware/tools
   AVRDUDE=$HARDWARE_TOOLS/avrdude64
   AVRDUDE_CONF=$HARDWARE_TOOLS/avrdude.conf
   sudo $AVRDUDE -C $AVRDUDE_CONF -P usb -p atmega328p -c avrisp2 -t -v


* -C avrdude.conf ファイルの指定。対象チップごとの情報が記述してある。
* -P アクセスするポートの指定。rootでusbと指定した場合は |avrdude| が適切なポートを探し出して接続してくれる。
* -p 指定チップ名の指定。 |uno| の場合は atmega328p を指定する。
* -c プログラマ(ISP)の指定。 |avrisp2| の場合は avrisp2 と指定。
* -t プロンプトを出して入力待ちになる。
* -v verbose。

|avrdude| を立ち上げます。sudoのパスワードを聞かれるので適宜入力してください。

.. code-block:: bash
   
   $ ./edit_uno.sh

signatrueが 0x1e950f なら正常に接続できています。0x000000 の場合は通信できていません。

lfuse, hfuse, efuse は表示されている値になっているので、指定します。

.. code-block:: none
   
   avrdude>w hfuse 0 0xd9

lock bitsは表示されていないので、まず表示します。

.. code-block:: none
   
   avrdude>d lock

次に設定します。

.. code-block:: none
   
   avrdude>w lock 0 0x3f

|avrdude| を終了します。

.. code-block:: none
   
   avrdude>quit


書き込み
========

ヒューズビット、ロックビットの設定と同様に upload_uno.sh というスクリプトを作ります。例えば以下のような感じです。

.. code-block:: bash

   #!/bin/bash
   HARDWARE_TOOLS=$HOME/.arduino/arduino/hardware/tools
   AVRDUDE=$HARDWARE_TOOLS/avrdude64
   AVRDUDE_CONF=$HARDWARE_TOOLS/avrdude.conf
   sudo $AVRDUDE -C $AVRDUDE_CONF -P usb -p atmega328p -c avrisp2 -U flash:w:uno.hex -U eeprom:w:uno.eep.hex -v

* -C avrdude.conf ファイルの指定。対象チップごとの情報が記述してある。
* -P アクセスするポートの指定。rootでusbと指定した場合は |avrdude| が適切なポートを探し出して接続してくれる。
* -p 指定チップ名の指定。 |uno| の場合は atmega328p を指定する。
* -c プログラマ(ISP)の指定。 |avrisp2| の場合は avrisp2 と指定。
* -U flashw:uno.hex カレントディレクトリのuno.hexをflashへ書き込む。
* -U eeprom:w:uno.epp.hex カレントディレクトリのuno.epp.hexをEEPROMへ書き込む。
* -v verbose。

\-U で指定した順番で書き込みが行われます。それぞれverifyを行い、違いがあった場合はそこで中断します。


動作確認
========
 
ISPを取り外します。

好きなターミナルでアクセスします。ここではbyobuを使っています。

.. code-block:: bash

   $ byobu /dev/ttyACM0

以下のとおりタイトル、プロンプトが出れば転送は出来ています(5.0のところはバージョンによって異なる)。

.. code-block:: none
   
   amforth 5.0 ATmega328p ForthDuino
   >

.. note::
   byobuを終了したい場合は、 プレフィックス k (CTRL+A k) で殺す。

.. image:: forthduino.png
   :width: 80%
   :align: center
