.. -*- coding: utf-8; mode: rst; -*-

.. include:: definition.txt

==============================
カスタマイズ(プリミティブ追加)
==============================

Users Guideから抜粋します。詳細はそちらをご覧下さい。

.. csv-table:: 関連incファイル
   :header: "ファイル名","配置領域","備考"
   :widths: 25, 25, 40
   
   "dict_appl.inc", "フラッシュ下位領域", "アドレス下位から上位方向に向かって伸びる"
   "dict_appl.core.inc", "NRWW領域", "ブートローダーセクション。unoの場合は0x3800〜0x3FFF"

NRWW領域はdict_wl.incを追加した時点でほぼ満杯なので、以後追加するものはdict_appl.incに追加することになります。

.. asmソース上ではLFAは意識しないでいいので、dict_appl.incの末尾に必要なwordのssmを追加するようにしてください。
   但し依存関係は注意してください。未定義のワードを参照しているとアセンブルに失敗します。

.. wordの検索は最末尾から開始されますが、それは、dict_appl.incをincludeした側で処理するので、dict_appl.incの中では気にする必要はありません。
   興味のある人はソースを追ってみて下さい。

.. なお、\*.frtの追加は今のところビルド済ませてから amforth-shell.py 等でアップロードします。まとめてアップロードするファイルを用意して
   おくと作業が楽になるでしょう。

ワード marker の追加
====================

uno用の構成ではデフォルトではワードの削除を行うワードを入れていません。つまり、定義しちゃうと消せなくて、
最初のバイナリ転送からやり直しになります。
フラッシュは書き込み回数制限もありますし、現在では母艦となるPCがあるのが当たり前ですのでこの開発スタイルで使いつづけても構いません。

バイナリイメージの再転送無しにワード削除も行いたいと言う場合はワードMARKERを定義します。
|amforth| では伝統的なFORGETと言うワードは準備してなくて、代わりにMARKERというワードを使います。

説明はamforthのサイトの RECIPES/Un-Doing Definitions にある通りですので
詳細はそちらを見ていただくとして、ここでいくつか補足します。

説明を見れば分かる通り、dict_wl.inc を追加してアセンブルしてから所定のFORTHワード定義をincludeする必要があります。

amforth-code/appl/arduino/dict_appl_core.inc の、 ;.include "dict_wl.inc" のコメント ; を外して make してください。

.. code-block:: bash

   $ make clean ; make uno.hex

makeしたバイナリイメージを実機に入れてから、

amforth-shell.py で登録するためのファイルを用意します。たとえばextends.frt

.. code-block:: none

   #include postpone.frt
   #inclue marker.frt
   marker sketches

といった具合です。これを amforth-shell.py で取り込みます。
