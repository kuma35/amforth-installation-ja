.. amforth-installation documentation master file, created by
   sphinx-quickstart on Fri Mar 22 10:19:48 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

amforth インストール (on Arduino uno R3)
========================================

Contents:

.. toctree::
   :maxdepth: 2
   :numbered:

   quickstart
   build
   customize

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

